/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.greglturnquist.payroll;

import java.time.LocalDate;
import java.time.Period;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import static java.time.LocalDate.now;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Entity // <1>
public class Employee {

	private @Id @GeneratedValue Long id; // <2>
	private String firstName;
	private String lastName;
	private String description;
	private String jobTitle;
	private String startDate;
	private int jobYears;
	private String email;

	private Employee() {}

	public Employee(String firstName, String lastName, String description, String jobTitle, String startDate, String email) {
		checkIfDataIsValid(firstName);
		this.firstName = firstName;
		checkIfDataIsValid(lastName);
		this.lastName = lastName;
		checkIfDataIsValid(description);
		this.description = description;
		checkIfDataIsValid(jobTitle);
		this.jobTitle = jobTitle;
		checkIfDataIsValid(startDate);
		this.startDate = startDate;
		this.jobYears = calculateJobYears(startDate);
		checkIfEmailIsValid(email);
		this.email = email;
	}

	/**
	 * Method to check if data is valid.
	 * Checks: not null & not empty.
	 *
	 * @param data data input
	 */
	protected static void checkIfDataIsValid(final String data) {
		if (data == null) {
			throw new IllegalArgumentException("Data cannot be null");
		}
		if (data.length() == 0) {
			throw new IllegalArgumentException("Data cannot be empty");
		}
	}

	/**
	 * Method to check if email data is valid.
	 * Checks: not null, not empty and correct email format.
	 *
	 * @param emailData data input
	 */
	protected static void checkIfEmailIsValid(final String emailData) {
		if (emailData == null) {
			throw new IllegalArgumentException("Data cannot be null");
		}
		if (emailData.length() == 0) {
			throw new IllegalArgumentException("Data cannot be empty");
		}

		final Pattern emailPattern = Pattern.compile(
				"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
				Pattern.CASE_INSENSITIVE);
		final Matcher emailMatcher = emailPattern.matcher(emailData);
		if (!emailMatcher.matches()) {
			throw new IllegalArgumentException("Email format is invalid");
		}

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public int getJobYears() {
		return jobYears;
	}

	private int calculateJobYears(String startDateString) {
		int answer;
		LocalDate startDateLocalDate = LocalDate.parse(startDateString);
		if (startDateLocalDate.isBefore(now())) {
			LocalDate thisMoment = now();
			LocalDate start = startDateLocalDate;
			Period period = Period.between(thisMoment, start);
			answer = Math.abs(period.getYears());
		} else {
			answer = 0;
		}
		return answer;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Employee employee = (Employee) o;
		return Objects.equals(id, employee.id) &&
				Objects.equals(firstName, employee.firstName) &&
				Objects.equals(lastName, employee.lastName) &&
				Objects.equals(description, employee.description) &&
				Objects.equals(jobTitle, employee.jobTitle) &&
				Objects.equals(startDate, employee.startDate) &&
				Objects.equals(email, employee.email);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, firstName, lastName, description, jobTitle, startDate, email);
	}

	@Override
	public String toString() {
		return "Employee{" +
			"id=" + id +
			", firstName='" + firstName + '\'' +
			", lastName='" + lastName + '\'' +
			", description='" + description + '\'' +
				", jobTitle='" + jobTitle + '\'' +
				", jobTitle='" + startDate + '\'' +
				", jobTitle='" + email + '\'' +
			'}';
	}
}
// end::code[]
