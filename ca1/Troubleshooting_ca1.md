# TROUBLESHOOTING #
### On Class Assignment 1 ###
A step-by-step tutorial for troubleshooting the VCS assignment

## Challenges ##
* Part 1: [Copying a Maven project](#PART-1:-Copying-a-Maven-project)
* Part 2: [Java compiler problems](#PART-2:-Java-compiler-problems)
* Part 3: [Installing SVN server](#PART-3:-Installing-SVN-server)

### Copying a Maven project

1) Copy of the "basic" version of the code "Tutorial React.js and Spring Data REST Application" in the IntelliJ environment in order to preserve the maven project structure with a refactoring of its name to "ca1".

> $ git commit -a -m "Add project folder ca1 with basic project\n\n Correct .iml file\n Create README_ca1.md for reporting on assignment"

[ ! ] Due to a miss configuration of the git core.editor this first commit has its line feeds interpreted as "\n". This is resolved with the command:

> git config --global core.editor "vim --nofork"

4) Create an issue (Issue #1) in Bitbucket Issues for the tracking of the development of an enhanced version of the app. It is to be used as a reference in future commit messages. A configuration is necessary in Bitbucket:
- [x] Go to Repository settings
- [x] Go to ISSUES
- [x] In Issue tracker: select Private issue tracker
- [x] Save

5) 


### Java compiler problems

1) Most of the compiling problems that occurred arose from a miss configuration in the system filepath for Java. Adding the correct Java filepath to Windows 10 "System Properties" (search for "Edit System Environment Variables").

![](C:\SWitCH projects\devops-21-22-atb-1211837\ca1\basic\images\system-properties.png)

2) Add JAVA_HOME Variable Name with "C:\Program Files\Java\jdk-11.0.14" <- your own filepath.

### Installing SVN server

1) Copy of the "basic" version of the code "Tutorial React.js and Spring Data REST Application" in the IntelliJ environment in order to preserve the maven project structure with a refactoring of its name to "ca1".

> $ git commit -a -m "Add project folder ca1 with basic project\n\n Correct .iml file\n Create README_ca1.md for reporting on assignment"

[ ! ] Due to a miss configuration of the git core.editor this first commit has its line feeds interpreted as "\n". This is resolved with the command:

> git config --global core.editor "vim --nofork"

4) Create an issue (Issue #1) in Bitbucket Issues for the tracking of the development of an enhanced version of the app. It is to be used as a reference in future commit messages. A configuration is necessary in Bitbucket:
- [x] Go to Repository settings
- [x] Go to ISSUES
- [x] In Issue tracker: select Private issue tracker
- [x] Save

5) 