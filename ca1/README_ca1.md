# Class Assignment 1 #
### Report on Version Control with Git ###
A step-by-step tutorial and assessment of version control software (VCS)

## Structure of the report ##
* Part 1: No branch implementation in Git
* Part 2: Branched implementation in Git
* Part 3: Branched implementation in SVN (Alternative VCS)

### PART 1: No branch implementation in Git ###

1) Copy of the "basic" version of the code "Tutorial React.js and Spring Data REST Application" in the IntelliJ environment in order to preserve the maven project structure with a refactoring of its name to "ca1".
2) With Git Bash (in the devops-21-22-atb-1211837 repository root), stage all files and commit them to the main branch repository.
> $ git add .
>
> $ git status
> 
> $ git commit -a -m "Add project folder ca1 with basic project\n\n Correct .iml file\n Create README_ca1.md for reporting on assignment"

[ ! ] Due to a miss configuration of the git core.editor this first commit has its line feeds interpreted as "\n". This is resolved with the command:

> git config --global core.editor "vim --nofork"

This way all future commit messages are edited in the Vim editor.


3) Make baseline changes to the original project adding a fourth filed (Job Title) committed, pushed all changes and tagged the version:
> $ git add .
>
> $ git status
>
> $ git commit -a -m "Add new jobTitle field"
> 
> $ git push
> 
> $ git tag -a v1.1.0 -m "baseline version"
> 
> $ git push origin v1.1.0


4) Create an issue (Issue #1) in Bitbucket Issues for the tracking of the development of an enhanced version of the app. It is to be used as a reference in future commit messages. A configuration is necessary in Bitbucket:
- [x] Go to Repository settings
- [x] Go to ISSUES
- [x] In Issue tracker: select Private issue tracker
- [x] Save

5) Develop all features in your codebase using your IDE of choice (IntelliJ was used for this assignment).


6) Test all changes running spring-boot and check at localhost//:8080

![](C:\SWitCH projects\devops-21-22-atb-1211837\ca1\basic\images\localhost.png)


7) Commit all changes and tag final work with v1.2.0:
> $ git add ca1/*
>
> $ git status
>
> $ git commit -a -m "Add new jobYears field with tests"
>
> $ git push
>
> $ git tag -a v1.2.0 -m "Job Years field"
>
> $ git push origin v1.2.0


9) Mark repo with tag ca1-part1:
> $ git tag -a ca1-part1 -m "CA1 part1 complete"
>
> $ git push origin ca1-part1


### PART 2: Branched implementation in Git ###

1) Create "email-field" branch and develop changes in the same basic project (cont.) with tests.
2) Commit all changes locally and commit the new branch (with changes) to bitbucket. 
3) With Git Bash (in the devops-21-22-atb-1211837 repository root), stage all files and commit them to the main branch repository.
> $ git branch
> <p> * main
>
> $ git branch email-field
>
> $ git checkout email-field
> 
> $ git branch
> <p> * email-field
> <p> main
> 
> $ git add ca1/*
>
> $ git status
>
> $ git commit
> 
> $ git push -u origin email-field
> 
> $ git checkout main
> 
> $ git merge email-field
> 
> $ git branch -v
> <p>email-field 840f696 Add email field with tests
> <p>* main        840f696 Add email field with tests
> 
Branch is merged and safe for deletion since we should not need it anymore.
> git tag v1.3.0
>
> git push origin v1.3.0
> 
> git tag
> <p>ca1-part1
> <p>v1.1.0
> <p>v1.2.0
> <p>v1.3.0

![](C:\SWitCH projects\devops-21-22-atb-1211837\ca1\basic\images\git-log_v1.3.0.png)

Deletion of disposable branch:
![](C:\SWitCH projects\devops-21-22-atb-1211837\ca1\basic\images\deleteBranch_email-field.png)

4) Create branch to fix a bug in email validation using a command that creates this branch and moves the HEAD to it.
> git checkout -b fix-invalid-email
> 

5) Add tests and code for email format validation and commit changes. Merge these changes, tag new version and delete branch that is no long necessary.  
6) Push changes to bitbucket server.
7) Delete fix-invalid-email branch since it is no longer necessary.

### PART 3: Branched implementation in SVN (Alternative VCS) ###
1) The SVN VCS was chosen for the alternative system to analyse. A server in DEI was setup using the commands referred to in the troubleshooting guide.
2) The main difference between this VCS and Git is that it is a centralized version control system. Git, on the other hand, uses multiple repositories (every user stores the whole project) and a central repository (or multiple central repositories).
