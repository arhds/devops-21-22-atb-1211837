package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void getJobYears_Test_past() {
        int expected = 3;
        String firstName = "Carl";
        String lastName = "Minion";
        String description = "aka BI-DÓ";
        String jobTitle = "emergency minion";
        LocalDate startDateData = LocalDate.now().minusYears(3);
        String startDate = startDateData.toString();
        String email = "carl@shire.lotr.com";
        Employee testEmployee = new Employee(firstName, lastName, description, jobTitle, startDate, email);

        int actual = testEmployee.getJobYears();

        assertEquals(expected, actual);
    }

    @Test
    void getJobYears_Test_lessThanOneYearAgo() {
        int expected = 3;
        String firstName = "Carl";
        String lastName = "Minion";
        String description = "aka BI-DÓ";
        String jobTitle = "emergency minion";
        LocalDate startDateData = LocalDate.now().minusYears(3);
        String startDate = startDateData.toString();
        String email = "carl@shire.lotr.com";
        Employee testEmployee = new Employee(firstName, lastName, description, jobTitle, startDate, email);

        int result = testEmployee.getJobYears();

        assertEquals(expected, result);
    }

    @Test
    void getJobYears_Test_now() {
        int expected = 0;
        String firstName = "Carl";
        String lastName = "Minion";
        String description = "aka BI-DÓ";
        String jobTitle = "emergency minion";
        LocalDate startDateData = LocalDate.now().minusDays(100);
        String startDate = startDateData.toString();
        String email = "carl@shire.lotr.com";
        Employee testEmployee = new Employee(firstName, lastName, description, jobTitle, startDate, email);

        int result = testEmployee.getJobYears();

        assertEquals(expected, result);
    }

    @Test
    void getJobYears_Test_future() {
        int expected = 0;
        String firstName = "Carl";
        String lastName = "Minion";
        String description = "aka BI-DÓ";
        String jobTitle = "emergency minion";
        LocalDate startDateData = LocalDate.now().plusYears(3);
        String startDate = startDateData.toString();
        String email = "carl@shire.lotr.com";
        Employee testEmployee = new Employee(firstName, lastName, description, jobTitle, startDate, email);

        int result = testEmployee.getJobYears();

        assertEquals(expected, result);
    }

    @ParameterizedTest
    @NullAndEmptySource
    void constructor_Test_startDate_NullEmpty(String input) {
        String firstName = "Carl";
        String lastName = "Minion";
        String description = "aka BI-DÓ";
        String jobTitle = "emergency minion";
        String email = "carl@shire.lotr.com";

        assertThrows(IllegalArgumentException.class,
                () -> new Employee(firstName, lastName, description, jobTitle, input, email));
    }

    @ParameterizedTest
    @NullAndEmptySource
    void constructor_Test_email_NullEmpty(String input) {
        String firstName = "Carl";
        String lastName = "Minion";
        String description = "aka BI-DÓ";
        String jobTitle = "emergency minion";
        String startDate = "2022-03-18";

        assertThrows(IllegalArgumentException.class,
                () -> new Employee(firstName, lastName, description, jobTitle, startDate, input));
    }

    @ParameterizedTest
    @ValueSource(strings = {"testATisep.ipp.pt", "email$isep.ipp.pt"})
    void constructor_Test_email_Fail(String input) {
        String firstName = "Carl";
        String lastName = "Minion";
        String description = "aka BI-DÓ";
        String jobTitle = "emergency minion";
        String startDate = "2022-03-18";

        assertThrows(IllegalArgumentException.class,
                () -> new Employee(firstName, lastName, description, jobTitle, startDate, input));
    }

    @ParameterizedTest
    @ValueSource(strings = {"test@isep.ipp.pt", "email@isep.ipp.pt"})
    void constructor_Test_email_Success(String input) {
        String firstName = "Carl";
        String lastName = "Minion";
        String description = "aka BI-DÓ";
        String jobTitle = "emergency minion";
        String startDate = "2022-03-18";
        Employee testEmployee = new Employee(firstName, lastName, description, jobTitle, startDate, input);

        String actual = testEmployee.getEmail();

        assertEquals(input, actual);
    }

}