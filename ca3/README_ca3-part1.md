# Class Assignment 3 #
### Report on Virtualization with Vagrant ###
A step-by-step tutorial and assessment of virtualization software

## Structure of the report ##
* Part 1: Virtualization using VirtualBox 
* Part 2: Virtualization with Vagrant
* Part 3: Virtualization with XXXX (Alternative virtualization tool)
<br/><br/>

### PART 1: Virtualization using VirtualBox ###

1) Install the VirtualBox software and configure a VM:
   <br/><br/>
   1) Installation of the VM with the minimal version of Ubuntu 18.04 (according to the class instructions) is straightforward but for an issue with the .ISO download: the download is made from an insecure site different/redirected from the original site. It should be verified using the md5/sha1 hash provided in the original Ubuntu page to be sure it hasn't been modified/tampered with.
      On the windows terminal (cmd.exe):
   >~$ certutil -hasfile mini.iso MD5  &nbsp;

   >~$ certutil -hasfile mini.iso SHA1  &nbsp;

      ![](images/ca3-1_checksum.png)
   <br/><br/>

   1) Installed VM pre-configuration:
      <br/><br/>
         ![](images/ca3-1_ubuntu-vm.png)
      &nbsp;

   2) Installed VM post-configuration according to the lecture 5 instructions:
      <br/><br/>
         ![](images/ca3-1_ubuntu-vm-configured.png)
      &nbsp;

   3) The chosen terminal / interface used for this configuration was an Ubuntu 20.04 LTS installation in Windows10 (WSL 2.0).
      <br/><br/>
         ![](images/ca3-1_terminal-ubuntu-wsl2.0.png)
      &nbsp;

2) Add the Spring Tutorial Application, build and run the application.
The tutorial app runs without issues on this setup.
> ~$ git clone https://github.com/spring-guides/tut-react-and-spring-data-rest
>
> ~$ cd tut-react-and-spring-data-rest/basic
> 
> ~$ ./mvnw spring-boot:run

Screenshot of the running app:
![](images/ca3-1_SpringTutorial-running.png)

3) Add the gradle-basic-demo project (from own repository), build and run the application.
   1) For a regular effective connection between custom vm servers and the bitbucket site it is advisable to configure an app specific password key. This is configured in the repository settings in bitbucket.
   2) Download the project from bitbucket with git clone command (password is requested):
      >     ~$ git clone https://ARHdS@bitbucket.org/arhds/devops-21-22-atb-1211837.git
   3) Move to the ca2-part1 directory where the gradle-basic-demo is. The vm server has no graphical user interface (GUI) so all configurations are made via vim editor in command line console. 
   4) Edit the runClient task in the build.gradle file to allow for connections to the specific ip of the vm server (192.168.56.5) instead of the localhost:
      >     task runClient(type: JavaExec, dependsOn: classes) {
      >         group = "DevOps"
      >         description = "Launches a chat client that connects to a server on localhost:59001"
      >         classpath = sourceSets.main.runtimeClasspath
      >         mainClass = 'basic_demo.ChatClientApp'
      >         args '192.168.56.5', '59001'
      >     }
   5) Before running the build it is necessary to change the permissions of gradlew:
      >     $ sudo chmod -R 700 gradlew
      >     $ ./gradlew build
   6) Run the server app:
      >     $ java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
   7) This solution allows for a simple, nimble vm server to deal with normal usage of the chat app since it has no graphical rendering to weight on its system. Open on your local machine two terminals and run the client app in each to test communication.
      >     $ ./gradlew runClient
      Screenshot of the working setup
      ![](images/ca3-1_ubuntu-vm_no-ui-server.png)
      <br/><br/>

4) The client side of the chat app cannot be run / tested inside the server because it has no GUI. This means development / testing requires at least two machines. In this case the client side machine has to have a GUI.