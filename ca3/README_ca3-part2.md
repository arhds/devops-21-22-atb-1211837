# Class Assignment 3 #

---
### Report on Virtualization with Vagrant ###

A step-by-step tutorial and assessment of virtualization software

---

## Structure of the report ##

* Part 2: Virtualization with Vagrant

* Annex: Troubleshooting issues

### PART 2: Virtualization using Vagrant ###

1) Install Vagrant and create a new project folder "vagrant-multi-spring-tut-demo":
   <br/><br/>
    1) Run vagrant init to start a new project.
    2) Edit the Vagrantfile to configure the VMs:
       ```Bash
       git clone https://ARHdS@bitbucket.org/arhds/devops-21-22-atb-1211837.git
       ```
    3) Due to permissions accessing the repository a read-only key was created in Bitbuckets "Personal settings" - "ACCESS MANAGEMENT" - "App passwords". The access in this way is possible without making the repository public and the password may be revoked at any time.
       ```Bash
       git clone https://ARHdS:WsUEA24aDgj7HHU2WKA4@bitbucket.org/arhds/devops-21-22-atb-1211837.git
       ```      
    4) Due to an error the modified project was substituted by the "tut-basic-gradle" and the original supplied Vagrantfile was kept unchanged but for the amount of memory of the web server:
       ```Bash
       # We set more ram memory for this VM
              web.vm.provider "virtualbox" do |v|
                 v.memory = 2048
              end
       ```    
       
       Edit the given Vagrantfile to correct a permission error:
       ![](images/ca3-2_chmod-error.png)
          
       Corrected adding "./" before the gradlew file permission change command:
       ```Bash
       chmod u+x ./gradlew
       ```

2) In a "git bash" command line on the "vagrant-multi-spring-tut-demo" folder run the command to start the build and configuration of the VMs
````Bash
vagrant up
````

<em>Result</em><br>



---

### ANNEX: Troubleshooting issues ###

1. Vagrant hangs at "SSH auth method: Private key":
   1. This issue may be due to interactions of Vagrant with "Docker desktop" or HyperV (all virtualization apps). However, it was solved by generating a new ssh key pair with "ssh-keygen".
      <br/>
      <br/>

2. Build failed with installNode error:
    > Task :installNode FAILED
    >
    >  FAILURE: Build failed with an exception.
    >
    > * What went wrong:
    > 
    >   Execution failed for task ':installNode'.
    > 
    >   Querying the mapped value of map(java.io.File task ':installNode' property 'nodeInstallDirectory' org.gradle.api.internal.file.DefaultFilePropertyFactory$ToFileTransformer@98f1c36) before task ':installNode' has completed is not supported

   1. This issue was not solved with the modified project. A decision was made to install the tut-basic-gradle in order to at least complete the assignment.
      <br/>
      <br/>
   
3. Any miss-configuration of the VMs can be corrected by editing the configuration sections of the Vagrantfile and running the command
    ```Bash
    vagrant reload --provision
    ```
    However, "vagrant up" tends to produce either a hanged VM or a non-functional system that ends up powered and unable to successfully recover. In these cases a cleanup process of a failed build is preferable with:

   ```Bash
      vagrant halt
      vagrant destroy -f
   ```

This will effectively shutdown the created VMs and eliminate them from the system.