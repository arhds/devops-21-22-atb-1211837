# Class Assignment 5 #

---

### Report on CI/CD Pipelines with Jenkins : CA5 - PART 1 ###


---

## Structure of the report ##

[Part 1: Installing Jenkins](#Part 1)

[Part 2: Configuring the pipeline](#Part 2)


[Troubleshoot: Issues with implementation](#Part 3)

---

<a id="part1"></a>
### Part 1: Installing Jenkins

1) Install the Jenkins software from https://jenkins.io/download/:
   <br/>
   1) Download the jenkins.war file (because it's for a Windows10 system).
   2) Copy the jenkins.war file to the ca5 directory of the local repository;
   3) Run the command from a terminal in this same directory:
   ```Bash
   java -jar jenkins.war
   ```
   4) Run Jenkins from the browser (localhost:8080) and configure the final installation accepting the default plugins.
   <br/><br/>
   <em>Install and Configuration success</em>
   <br>
   ![](images/ca5-part1_jenkins_installComplete.png)
   <br/>
   <br/>

      
---

<a id="part2"></a>
### Part 2: Configuring the pipeline


1) Create new credentials:
   <br/>
   1) Go to "Manage Jenkins".
   2) Choose "Manage credentials".
   3) In "Stores scoped to Jenkins" select (global) Domain:
   4) Add Credentials for bitbucket access (side menu). Save the ID name used here for later use.
      <br/>
      <br/>

   This step was rendered useless since it proved impossible to keep the repo private and work with these credentials. It was decided that a more effective strategy was to make the repo public.
<br/>
<br/>

2) Create a new pipeline:
   <br/>
   1) Create a job.
   2) Choose new pipeline option.
   3) Scroll down to "Advanced Project Options" and select:
      1) Definition: "Pipeline script from SCM"
      2) SCM: "Git"
      3) Repository URL: "https://ARHdS@bitbucket.org/arhds/devops-21-22-atb-1211837/"
      4) Credentials: none (the repo is public)
      5) Branch Specifier: "*/master"
      6) Script Path: "ca5/gradle-basic-demo/Jenkinsfile"
   4) Apply changes and save.
      <br/>
      <br/>

   Note: In the branch specifier "master" was the only viable choice. It had been thoroughly tested with the original branch name (main) and no result was possible. The recurrent log message was:
   ````Bash
   The recommended git tool is: NONE
   No credentials specified
   > git.exe rev-parse --resolve-git-dir C:\Users\andre\.jenkins\workspace\ca5-part1\.git # timeout=10
   Fetching changes from the remote Git repository
   > git.exe config remote.origin.url https://ARHdS@bitbucket.org/arhds/devops-21-22-atb-1211837 # timeout=10
   Fetching upstream changes from https://ARHdS@bitbucket.org/arhds/devops-21-22-atb-1211837
   > git.exe --version # timeout=10
   > git --version # 'git version 2.33.0.windows.2'
   > git.exe fetch --tags --force --progress -- https://ARHdS@bitbucket.org/arhds/devops-21-22-atb-1211837 +refs/heads/*:refs/remotes/origin/* # timeout=10
   > git.exe rev-parse "refs/remotes/origin/master^{commit}" # timeout=10
   > git.exe rev-parse "origin/master^{commit}" # timeout=10
   ````
   This may indicate a bug in the Jenkins Branch Specifier subroutine. Once the main branch of the bitbucket repository was changed to "master" the checkout phase was immediately solved.
   <br/>
   <br/>
   <br/>
3) Run this job pressing the "Build Now" button.
   <br/><br/>
   <em>Install and Configuration success</em>
   <br>
   ![](images/ca5-part1_pipeline-success.png)
   <br/>
   <br/>

---