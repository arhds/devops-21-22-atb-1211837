# Class Assignment 2 #
### Report on Build Tools with Gradle ###
A step-by-step tutorial and assessment of Build Tools

## Structure of the report ##
* Part 2: Build Tools with Gradle

### PART 2: Build Tools with Gradle ###

1) Create a new branch for this assignment named "tut_basic_demo":
> $ git checkout -b tut-basic-gradle

Screenshot of successful branch creation for development of CA2-part2:
![](images/tut-basic-demo-branch.png)
2) Initialize the project manually from scratch in https://start.spring.io/ as per the tutorial basic demo project README.adoc (and in the rest service guide https://spring.io/guides/gs/rest-service/).
<p>
Setting used were those of the assignment instruction with only one variation:

> Spring Boot 2.6.7

3) Execute instructions 3 to 12 of the CA2, Part2 assignment. These end with the build:
    ```Bash
    ./gradlew build
    ```
   And the execution of the application:
   ```Bash
    ./gradlew bootRun
   ```

   Screenshot of application running:
![](images/tut-basic-demo-base-version.png)


13) Add a task to copy the generated JAR file to a "dist" folder (root level):
>     //New task copyJAR
>     task copyJAR(type : Copy){
>
>         // Source and target
>         from "build/libs"
>         include "*.jar"
>         into "dist"
>     }

with alternative directory syntaxe:
>     //Register new task copyJAR
>     tasks.register ('copyJAR1', Copy){
>
>         // Source and target
>         from layout.buildDirectory.dir("libs")
>         include "*.jar"
>         into layout.projectDirectory.dir("dist")
>     }

Assuming this code is a way to place the distribution files of the latest build in a specific folder the following line was added to build-gradle:
>     build.dependsOn copyJAR
This way every time a build is made the distribution files are updated in the "dist" folder.

14) Add a task to delete the generated files of the webpack:
>     //Create new task to delete webpack generated files
>     tasks.register ('cleanWP', Delete) {
>
>     // Source and target
>     doFirst{
>         delete "src/main/resources/static/built/"
>     }

Since this task is to be run before the task clean the following code was added to build.gradle:
>     clean.dependsOn cleanWP




7) Finalize work merging the branch (fast-forward) and apply the ca2-part2 tag to the final commit.
> $ git commit
>
> $ git push origin tut-basic-gradle
>
> $ git checkout 
> 
> $ git merge tut-basic-gradle
> 
> $ git push origin main
> 
> $ git tag ca2-part2
>
> $ git push origin ca2-part2