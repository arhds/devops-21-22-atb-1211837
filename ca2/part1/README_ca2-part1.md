# Class Assignment 2 #
### Report on Build Tools with Gradle ###
A step-by-step tutorial and assessment of Build Tools

## Structure of the report ##
* Part 1: Build Tools with Gradle

### Part 1: Build Tools with Gradle ###

1) Download of the code "gradle_basic_demo" via git.
> $ git clone https://bitbucket.org/luisnogueira/gradle_basic_demo/
2) Run the projects README.md suggested build and server running commands in the IntelliJ terminal to test the application.
> $ ./gradlew build
>
> $ java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
>
Run the client commands in different terminals:
> $ ./gradlew runClient

Screenshot of application test using the terminals "git bash" and "Windows PowerShell":
![](images/gradle-basic-demo-chat.png)

3) Add an executeServer task to the build.gradle configuration file:
>     //New task to execute server
> 
>     task executeServer(type:JavaExec, dependsOn: classes){
> 
>     group = "DevOps"
> 
>     description = "Launches a chat server that allows client chats to communicate on localhost:59001"
>
>        classpath = sourceSets.main.runtimeClasspath
>
>        mainClass = 'basic_demo.ChatServerApp'
>
>        args '59001'
>     }

4) Add the given unit test to a test folder and update the dependencies in the build.gradle configuration file to include JUnit4 for the test code. The "testImplementation" extends "implementation", meaning an Implementation phase only dependency for tests:
>     dependencies {
>          // Use Apache Log4J for logging
>          implementation group: 'org.apache.logging.log4j', name: 'log4j-api', version: '2.11.2'
>          implementation group: 'org.apache.logging.log4j', name: 'log4j-core', version: '2.11.2'
>
>          // Use JUnit4 for testing and configure its context: testImplementation extends implementation, meaning an Implementation phase only dependency for tests.
>          testImplementation 'junit:junit:4.13.1'
>     }

Screenshot of successful test and running application:
![](images/gradle-basic-demo-chat_test.png)

5) Add a backupSource Copy type task to the build.gradle configuration file:
>     //New task to backup the source code into a backup folder
>     task backupSource(type: Copy) {
>         group = "DevOps"
>         description = "Executes a backup copy of the source code into a backup folder"
>
>         // Copy instructions - source and target directories are defined in gradle.properties
>         from "${srcDir}"
>         into "${targetDir}"
>     }
> 
No directory creation command (mkdir) is necessary here since the Copy task performs the creation of the necessary directories if non-existent.
<p>

Defining the source and target directories in the gradle.properties file:
>     # Definition of Source and Target directories for backupSource task
>     srcDir=./src
>     targetDir=./backup
This task creates a new backup folder (if it doesn't exist) and backups all files in the src folder into the target folder updating/overwriting existing files.

Screenshot of successful backup Copy task:
![](images/gradle-basic-demo-backup-task.png)

6) Add an archive backup Zip type task:
>     //New task to backup the source code into a Zip archive in the build/backup folder
>     task backupZipSource(type: Zip) {
>         group = "DevOps"
>         description = "Executes a backup copy of the source code into a Zip file in the build/backup folder"
>
>         // Source of files to zip, final Archive filename and Target directory
>         from "/src"
>         archiveFileName = "backup.zip"
>         destinationDirectory = layout.buildDirectory.dir('backup')
>     }
Since these backups are associated with a specific build the target directory was changed to reflect this (target directory is defined in the build.gradle configuration file as "destinationDirectory").
<p>
This task thus creates a new backup folder in the build directory (if it doesn't exist) and backups all files of the src folder into the target folder in the form of a zip file. It updates/overwrites any existing zip file with the same name.

Screenshot of successful backup Zip task:
![](images/gradle-basic-demo-backupzip-task.png)

7) Apply the ca2-part1 tag to the final commit.
> $ git commit
> 
> $ git push origin main
> 
> $ git tag ca2-part1
> 
> $ git push origin ca2-part1