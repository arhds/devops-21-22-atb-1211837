# Class Assignment 4 #

---

### Report on Containers with Docker ###
Usage of Container software

---

## Structure of the report ##

[Part 1: Installing Docker](#Part 1)

[Part 2: Chat App - Building jar file version](#Part 2)

[Part 3: Chat App - Copying jar file version](Part 3)


---

<a id="part1"></a>
### Part 1: Installing Docker

1) Install the Docker software:
   <br/>
      On the Docker.com site:
   1) Download the Docker Desktop version (Windows 10) of the Docker software.
   2) Configure the Docker Settings (Dashboard) to use the WSL 2 engine (much better performance than Hyper-V).
   3) Register in Docker Hub.
      <br/><br/>
      ![](images/ca4-part1_install-docker.png)
   <br/>
   <br/>
---

<a id="part2"></a>
### Part 2: Chat App - Building jar file version

<em>Dockerfile</em><br>
```Dockerfile
FROM ubuntu:18.04
RUN apt-get update -y
RUN apt-get install -y openjdk-8-jdk-headless
RUN apt-get install git -y

ENV HOME /root
WORKDIR $HOME
RUN git clone https://bitbucket.org/luisnogueira/gradle_basic_demo.git
WORKDIR  $HOME/gradle_basic_demo

RUN chmod u+x gradlew
RUN ./gradlew clean build

EXPOSE 59001

CMD java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
```

<br>
<em>Building image from docker file</em><br>

```bash
docker build --no-cache -t ca4-part1-a .
```

<br>
<em>Resulting image</em>

![](images/ca4-part1_docker-image-a.png)

<br>
<em>Running the Chat Server</em>
<br>

- Once the container is created it is important to start it explicitly instructing the port:59001 to be exposed:

```bash
docker run -p 59001:59001 -d ca4-part1-a
```
![](images/ca4-part1_docker-container-a.png)

<br>
<em>Running the Client Chat Service</em>
<br>

- A custom startup command is run from the projects' directory instead of the usual runClient command to avoid any original network address configuration from the project:

```bash
java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatClientApp localhost 59001
```

<br>
<em>Functioning Setup - building jar file version </em>
<br>

![](images/ca4-part1_docker-ChatApp-running-a.png)

</br>

---

<a id="part3"></a>
### PART 3: Chat App - Copying jar file version

<em>Dockerfile</em><br>
```Dockerfile
FROM openjdk:11

WORKDIR /root
COPY /basic_demo-0.1.0.jar .

EXPOSE 59001

CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
```

<br>
<em>Building image from docker file</em><br>

```bash
docker build --no-cache -t ca4-part1-b .
```

<br>
<em>Resulting image</em>

![](images/ca4-part1_docker-images.png)

<br>
<em>Running the Chat Server</em>
<br>

- Once the container is created it is important to start it explicitly instructing the port:59001 to be exposed:

```bash
docker run -p 59001:59001 -d ca4-part1-b
```
![](images/ca4-part1_docker-container-b.png)

<br>
<em>Running the Client Chat Service</em>
<br>

- Once again a custom startup command is run from the projects' directory instead of the usual runClient command to avoid any original network address configuration of the project:

```bash
java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatClientApp localhost 59001
```

<br>
<em>Functioning Setup - copying jar file version </em>
<br>

![](images/ca4-part1_docker-ChatApp-running-b.png)
</br>
</br>

- Uploading images to Docker Hub requires an account login, image tagging and image pushing to the repository:
```bash
docker tag ca4-part1-a:latest arhds/switch:ca4-part1-a
docker push arhds/switch:ca4-part1-a
docker tag ca4-part1-b:latest arhds/switch:ca4-part1-b
docker push arhds/switch:ca4-part1-b
````

These images are now publicly accessible at https://hub.docker.com/repository/docker/arhds/switch

<br>
<em>Uploaded images</em>
<br>

![](images/ca4-part1_docker-images-hub.png)

---