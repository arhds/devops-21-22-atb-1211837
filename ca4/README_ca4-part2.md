# Class Assignment 4 #

---

### Report on Containers with Docker Part 2 ###
Usage of Container software

---

## Structure of the report ##

[Part 1: Building the Images and Containers](#Part 1)

[Part 2: Analysis of alternative](#Part 2)

[Part 3: Implementing the alternative](Part 3)


---

<a id="part1"></a>
### Part 1: Building the Images and Containers

1) Due to build/compile issues with the customized app the original repo version was used. A new project was created in a local projects folder:
    ```Bash
    git clone https://bitbucket.org/atb/docker-compose-spring-tut-demo.git
    ```

<br>

2) The docker-compose.yml file and the Dockerfile of each planned machine (db & web) were viewed to match the lecture versions.

<br>

3) Build images and run containers of the project for the first time using the WSL Linux shell (Ubuntu 20.04) on the project folder (where the docker-compose.yml file is):
<br/>
    ```Bash
    docker-compose up
    ````

    <br>
    <em>Running containers</em>
    <br>
    
    ![](images/ca4-part2_docker-compose-containers-running.png)

    <br>

4) Test the app by accessing it in the browser: 
    ```html
    http://localhost:8080/basic-0.0.1-SNAPSHOT/
    ```

<br>

5) Test the database by  accessing in the browser:
    ```html
    http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console
    ```
      connection string: jdbc:h2:tcp://192.168.33.11:9092/./jpadb
      
      user: sa
      
      [with no password]

    <br>
    <em>Test images</em>
    <br>
    
    ![](images/ca4-part2_docker-compose-app-running.png)

    <br>

6) Uploading images to Docker Hub requires account login, image tagging and image pushing to the repository:
    ```bash
    docker tag docker-compose-spring-tut-demo_db:latest arhds/switch:docker-compose-spring-tut-demo_db
    docker push arhds/switch:docker-compose-spring-tut-demo_db
    docker tag docker-compose-spring-tut-demo_web:latest arhds/switch:docker-compose-spring-tut-demo_web
    docker push arhds/switch:docker-compose-spring-tut-demo_web
    ````

    These images are now publicly accessible at https://hub.docker.com/repository/docker/arhds/switch
    
    <br>
    <em>Uploaded images</em>
    <br>
    
    ![](images/ca4-part2_docker-compose-images-hub.png)
   <br>
   <br>

7) Copying the database file to the shared volume folder:
    <br>
    <br>
    <em>Check images in system with the command</em>
    ```Bash
    docker ps
    ```
    ![](images/ca4-part2_docker-images.png) 
    <br>
    <em>Login into db vm</em>
    <br>
    ```Bash
    docker exec -it ee237e40d2b0 bash
    ```
    <br>
    <em>Copy database 'jpadb.mv.db' to the shared volume folder '/usr/src/data-backup' </em>
    <br>
   
    ```Bash
    docker exec -it ee237e40d2b0 bash
    ```

   ![](images/ca4-part2_docker-copy-db-into-sync-folder.png)

   ![](images/ca4-part2_docker-copy-db-into-sync-folder-win10.png)
---


<a id="part2"></a>
### Part 2: Analysis of alternative

<br>

Kubernetes is often viewed as an extension of the Docker capabilities although it can also be an alternative (of sorts).
<br>
<br>
A more obvious alternative to Docker would be "Podman" (https://podman.io/) which approaches containerization via a daemonless container engine. It answers the need for a more secure deployment since you no longer can use su/sudo with these rootless podman containers.
<br>
<br>
Back to Kubernetes (aka K8s), it is known and favoured for:
- Automated rollout (and rollback) of containerized apps and configurations
- Service discovery and load-balancing across a set of Pods (or a kubernetes cluster)
- Batch execution of CI/CD workloads
- Scaling deployment capabilities
- Monitor health and automate recovery, maintenance and removal of containers

and many more features...
<br>
<br>
This high flexibility, feature rich, cloud native tool has one major drawback: it is highly complex. It is relatively easy to use although difficult to master.
<br>
<br>
Additionally Kubernetes supports the management of different container/virtualization options (including Docker) simultaneously. 
---

<a id="part3"></a>
### PART 3: Implementing the alternative

<br>


---