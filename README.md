# REPORTS #

This README shows the structure and how to navigate the different projects and reports, both imported and modified (Class Assignment #), in this repository.

## Projects in repository ##

* [ca1](ca1/basic/src)
* [ca2 - part1](ca2/part1/basic/src)
* [ca2 - part2](ca2/part2/basic/src)
* ca3 (project made outside of the repository)
 


### How do I view their respective reports? ###

Just follow the links:

* [ca1](ca1/README_ca1.md) : Class Assignment 1
* [ca2-part1](ca2/part1/README_ca2-part1.md) : Class Assignment 2 part 1
* [ca2-part2](ca2/part2/react-and-spring-data-rest-basic/README_ca2-part2.md) : Class Assignment 2 part 2
* [ca3-part1](ca3/README_ca3-part1.md) : Class Assignment 3 part 1
* [ca3-part2](ca3/README_ca3-part2.md) : Class Assignment 3 part 2



###  Troubleshooting configuration/technical issues ###

* [Copying a maven project](ca1/Troubleshooting_ca1.md)
* [Java compiler problems](ca1/Troubleshooting_ca1.md)
* [Installing SVN server](ca1/Troubleshooting_ca1.md)
